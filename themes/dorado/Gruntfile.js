'use strict';

function getMessage() {
    var messages = [
        'I love you',
        'You\'re awesome',
        'Like a boss',
        'Kick ass',
        'This is why you make the big bucks',
        'Perfectly placed semicolons chief',
        'Wow. Nice bro',
        'You must do this a lot',
        'Such wow',
        'Get to the choppah!',
        'Pretty sweeeeet',
        'This better work',
        'Let\'s get foods',
        'You\'re my favorite dev',
        'No errors... Let\'ts just hope you\'re targeting the right thing',
        'Cowabunga',
        'Bangarang',
        'The important thing is that you are safe',
        'Ain\'t nobody got time for that'
    ];

    var message = messages[ Math.floor( Math.random() * messages.length) ];
    return '😍  ' + message;
}

module.exports = function(grunt) {

// load all grunt tasks matching the `grunt-*`
// grunt.loadNpmTasks('grunt-XXXX')

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),


//==============|  MAIN WATCH FUNCTION (dev) |===============================================
        watch: {
            //-------| WATCH LESS > CSS |----------------
            less: {
                files: [
                    'assets/less/*.less',
                    'assets/less/bootstrap/*.less'
                ],
                tasks: ['less:dev', 'notify:watch']
            },

            //-------| WATCH JS  > MINIFY  |----------------
            js: {
                files: [
                    'assets/js/_*.js'
                ],
                tasks: ['uglify', 'notify:watch']
            }

            //-----------| LIVE RELOAD |--------------< disabled cause we are using browser sync
//            livereload: {
//                // Browser live reloading https://github.com/gruntjs/grunt-contrib-watch#live-reloading
//                options: {
//                    livereload: false
//                },
//                files: [
//                    'assets/css/main.min.css',
//                    'assets/js/scripts.min.js',
//                    'templates/*.php',
//                    '*.php'
//                ]
//            }

        },

//==================| OTHER FUNCTIONS |==========================================================


        //----- Checks code for issues ----
        jshint: {
            options: {
                jshintrc: '.jshintrc'
            },
            all: [
                'Gruntfile.js',
                'assets/js/*.js',
                '!assets/js/scripts.min.js'
            ]
        },

        //----- Deletes files (useful for temps) -----
        clean: {
            css: [
                'assets/css/tmp/*.css'
            ]
        },

        //----- Convert less to css ---
        less: {
            dev: {
                files: {
                    'assets/css/editor-style.css': ['assets/less/editor.less' ],
                    'assets/css/main.min.css': ['assets/less/app.less' ]

                },
                options: {
                    compress: false, //Compress output by removing some whitespaces.
                    relativeUrls: true,
                    ieCompat: true, //Enforce the css output is compatible with Internet Explorer 8.

                    // LESS source map
                    // To enable, set sourceMap to true and update sourceMapRootpath based on your install
                    sourceMap: true,
                    sourceMapFilename: 'assets/css/main.min.css.map',
                    sourceMapRootpath: '/app/themes/graphicfusion/'
                }

            },
            //---------- GoLive options
            golive: {
                files: {
                    'assets/css/main.min.css': [
                        'assets/less/app.less'
                    ]
                },
                options: {
                    compress: true, //Compress output by removing some whitespaces.
                    cleancss: true,
                    relativeUrls: true,
                    ieCompat: true, //Enforce the css output is compatible with Internet Explorer 8.
                    sourceMap: false

                }

            }

        },

        //----- Makes files smaller by removing spaces---
        uglify: {
            dist: {
                files: {
                    'assets/js/scripts.min.js': [ //add all the scripts here
                       // 'assets/js/vendor/bootstrap.min.js', //uncomment this to include everything you MUST COMMENT OTHER ONES BELOW
                        'assets/js/plugins/*.js',

                       //---selective bootstrap loading-------------
                       //  'assets/js/plugins/bootstrap/affix.js',  // sticks things to top left etc, using .affix syntax
                       //  'assets/js/plugins/bootstrap/alert.js',  // alert and buttons to dismiss them
                       //  'assets/js/plugins/bootstrap/button.js', // allows buttons to change state, like adding "loading" to a button on click
                        'assets/js/plugins/bootstrap/carousel.js',  // rotating slider support
                        'assets/js/plugins/bootstrap/collapse.js',  // accordion support REQUIRES transition.js
                        'assets/js/plugins/bootstrap/dropdown.js',  // dropdown menus
                       //  'assets/js/plugins/bootstrap/modal.js',      // pop up box with your content REQUIRES modals.less (in bootstrap.less)
                       //  'assets/js/plugins/bootstrap/popover.js',    // popup box when you click a link REQUIRES tooltip.js! and popovers.less
                       //  'assets/js/plugins/bootstrap/scrollspy.js',  // be aware of the scroll positions, like if you want link highlight as you scroll
                        'assets/js/plugins/bootstrap/tab.js',           // tabs bro
                       // 'assets/js/plugins/bootstrap/tooltip.js',     // tooltips, enable tooltips.less in bootstrap.less
                        'assets/js/plugins/bootstrap/transition.js',

                        'assets/js/_*.js'
                    ]
                },
                options: {
                    mangle: true //set to false to prevent changes to your variable and function names
                    // JS source map: to enable, uncomment the lines below and update sourceMappingURL based on your install
                    // sourceMap: 'assets/js/scripts.min.js.map',
                    // sourceMappingURL: '/app/themes/roots/assets/js/scripts.min.js.map'
                }
            }
        },

        // ---- CONCAT JS -------------
//    concat: {
//          options: {
//              separator: ';',
//              banner: '/*! \nJS for <%= pkg.name %> - v<%= pkg.version %>\nAuthor: <%= pkg.author %>\nModified: <%= grunt.template.today("yyyy-mm-dd") %>\n */\n'
//          },
//          dist: {
//              // src: ['src/**/*.js'],
//              // dest: 'static/js/all.min.js'
//              // We're explicit in what files/order we concat...
//              src: ['assets/js/scripts.min.js'],
//              dest: 'httpdocs/static/js/app.min.js'
//          }
//      },



        //----- adds browser specific prefixes ----
        autoprefixer: {

            sm_update_by_path:  {
                options: {
                    browsers: ['last 3 versions', '> 1%', 'ie 8', 'ie 7'],
                    map: {
                        prev: 'assets/css/'
                    }
                },
                src: 'assets/css/main.min.css',
                dest: 'assets/css/main.min.css'
            }
        },


        //---- syncs changes between browsers ----
        browserSync: {
            dev: {
                bsFiles: {
                    src: ['assets/css/*.css','assets/js/*.js', 'assets/img/*', '**/*.php' ]

                },
                options: {
                    watchTask: true,
                    //  reloadDelay: 5000,

                    tunnel: 'base',
                    open: "external",
                    timestamps: false,
                    proxy: 'base.dev'
                }
            }
        },

        //---- checks for grunt modules and should install them ----
        required: {
            libs: {
                options: {
                    install: true
                },
                // Search for require() in all js files in the src folder
                src: ['src/*.js']
            }
        },

        //----- minify images --------------
        imagemin: {
            png: {
                options: {
                    optimizationLevel: 7
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: [
                            'assets/img/'
                        ],
                        src: ['**/*.png'],

                        //optimize upload folder too
                        dest: 'assets/img/',
                        ext: '.png'
                    }
                ]
            },
            jpg: {
                options: {
                    progressive: true
                },
                files: [
                    {
                        // Set to true to enable the following options…
                        expand: true,
                        // cwd is 'current working directory'
                        cwd: [
                            'assets/img/'
                        ],
                        src: ['**/*.jpg'],


                        dest: '/assets/img/',
                        ext: '.jpg'
                    }
                ]
            }
        },

        //------ System notifications --------------
        notify: {

            watch: {
                options: {
                    title: getMessage(),            // optional
                    message: 'Less files compiled ' // required
                }
            },
            server: {
                options: {
                    message: 'Minified, autoprefixed and ready to go live!'
                }
            }
        }
    });

    // Register tasks
    grunt.registerTask('imagepng', ['imagemin:png']); // only .png files
    grunt.registerTask('imagejpg', ['imagemin:jpg']); // only .jpg files

    grunt.registerTask('default', [
        'required',
        'less',
        'uglify'
    ]);

    grunt.registerTask('dev', ["browserSync", "watch"]);
    grunt.registerTask('dev-nosync', ["watch"]);
    grunt.registerTask('golive', [ //not working yet
        'less:golive',
        'autoprefixer',
        'uglify',
        'clean',
        'notify:server'
    ]);

};
