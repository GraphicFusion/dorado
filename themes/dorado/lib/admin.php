<?php

add_action( 'admin_init', 'egg_dependencies' );

/**
 * Check for dependencies
 */
function egg_dependencies()
{
	if ( ! class_exists( 'acf' ) )
	{
		add_action( 'admin_notices', 'egg_acf_dependency_message' );
	}
}


/**
 * Add a nag for required dependencies that are missing
 */
function egg_acf_dependency_message() { ?>
	<div class="update-nag">
		This theme requires the <a href="http://wordpress.org/plugins/advanced-custom-fields/">Advanced Custom Fields</a> plugin to be installed and activated.
	</div>
<?php } 

	/**
	 * Setup Plugin List to only show for Admin
	 */
	function hidePlugins() {
		global $wp_list_table;
		$hidearr = array(
			'wp-migrate-db-pro/wp-migrate-db-pro.php',
			'advanced-custom-fields-pro/acf.php'
		);
		$myplugins = $wp_list_table->items;
		foreach ($myplugins as $key => $val) {
			if (in_array($key,$hidearr)) {
				unset($wp_list_table->items[$key]);
			}
		}
	}
	$admin = false;
	if ( is_user_logged_in() ) {
		global $user_id;
		$user = new WP_User( $user_ID );
		if ( !empty( $user->roles ) && is_array( $user->roles ) ) {
			foreach ( $user->roles as $role )
				if( $role == 'administrator' ){
					$admin = true;
				}
		}
	}
	if( !$admin) {
		add_action( 'pre_current_active_plugins', 'hidePlugins' );
	}