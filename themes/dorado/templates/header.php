<header class="navbar navbar-fixed-top navbar-inverse " role="banner" id="themeslug-navbar">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="sidebar" data-target=".mobilemenu-one">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			
				<?php if (has_nav_menu('secmobile-menu')) : //-----| Second mobile menu (if on) ?>

					<button type="button" class="navbar-toggle toggle-two" data-toggle="sidebar" data-target=".mobilemenu-two">
						<span class="sr-only">Toggle Shop</span>
						<div class="icon-cart2"></div>				
					</button>

				<?php endif ?>

			<div class="navbar-brand">
				<a class="header-logo" href="<?php echo home_url(); ?>">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt=""/ ">
				</a>
			</div>
		</div>
		<nav class="navbar-collapse collapse" role="navigation">

			<?php
				if (has_nav_menu('primary_navigation')) {
					wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'nav navbar-nav'));
				};
			?>
		</nav>
	</div>
</header>
<div id='header-clone'></div>
<script>var offsetHeight = document.getElementById('themeslug-navbar').offsetHeight + "px"; document.getElementById('header-clone').style.height = offsetHeight; document.getElementsByTagName("body")[0].style.marginTop = offsetHeight;</script>