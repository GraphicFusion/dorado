The Filtered Image Div:
1. in the following files search and replace "prefix" with a unique id if you are using multiple types of sizes, etc
2. The containers must all have a defined height. Ids are: 'prefix-ow','prefix-obg', 'prefix-o', 'prefix-oc'; Filter-div has a demo script to auto-set the height; 
3. In functions.php, add the following line to the $dorado_includes (and update "prefix"): 	
	'templates/prefix-image-filter/filter-functions.php'	
4. To add custom fields, look in templates/prefix-image-filter/filter_functions.php; Custom fields are preregistered 
	