if(typeof jQuery!="undefined"){
	var $ = jQuery;
	if( $('.acf-field-dorado-add-sidebar input').is(':checked') ){
		$('.acf-field-use-custom-sidebar').show();
	}
	$(document).ready(function() { 
		$('.acf-field-dorado-add-sidebar input').on('change', function(){
			if($(this).is(':checked') ){
				$('.acf-field-use-custom-sidebar').show();
			}
			else{
				$('.acf-field-use-custom-sidebar').hide(); // and uncheck
				$('.acf-field-use-custom-sidebar input').attr('checked', false); 
			}
		});
	}); 
}